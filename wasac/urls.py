from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static

handler404 = 'dashboard.views.handler404'
# handler500 = 'my_app.views.handler500'

urlpatterns = [
    # Examples:
    # url(r'^$', 'wasac.views.home', name='home'),
    #
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', auth_views.login, {'template_name': 'login.html'}, name='login'),
    url(r'^login/$', auth_views.login, {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/login'}, name='logout'),
    url(r'^$', include('dashboard.urls')),
    url(r'^client/', include('client.urls')),
    url(r'^product/', include('product.urls')),
    url(r'^sell/', include('sell.urls')),
    url(r'^arduino/', include('Arduino.urls')),
    url(r'^dashboard/', include('dashboard.urls')),
    url(r'^ussd/', include('ussd.urls')),
]
