from rest_framework import serializers
from .models import SellWater, Insertedcode
from client.models import Client
from product.models import Product
import pandas as pd
from pandas import DataFrame
import numpy as np
import random
from Arduino.models import ArduinoCreation
import uuid
total = 1 
phone = 0



class CreateArduino(serializers.ModelSerializer):
    class Meta:
        model = ArduinoCreation
        fields = ['code']


class CodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Insertedcode
        fields = ['code']
    
class SellOutputSerializer(serializers.ModelSerializer):
    class Meta:
        model = SellWater
        fields = ['id', 'quantity', 'consumed']
        
        def Getconsumed(self, data):
            global quantity
            quantiy = data['quantity'] - data['consumed']
            return (data['consumed'])



        
class Sellserializer(serializers.ModelSerializer):

    class Meta:
        model = SellWater
        fields = ['id','client','product', 'machineId' ,'quantity','total_price', 'code', 'consumed', ]

    
    def code(self):
        unique_code = str(uuid.uuid4().int)[:8]
        code = unique_code
        return code

    def totalPrice(self, data):
        global total
        sell = SellWater.objects.all()
        for p in sell:
            if p.product.id == data['product']:
                total = p.product.price * data['quantity']            
        return total

    def Getquantity(self, data):
        global quantity
        quantiy = data['quantity'] - data['consumed']
        return (data['quantity']  - data['consumed'])

    def Paymentcode(self):
        vet = [random.randint(1,10) for _ in range(6)]
        a = vet
        code = ''.join(str(e) for e in a)
        return code

    def getClient(self, data):
        sell = SellWater.objects.all()
        global phone
        for c in sell:
            if c.client.id == data['client']:
                phone = c.client.phone_number
        return phone

    # if c.client.id == data['client']:

