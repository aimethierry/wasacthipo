from django.contrib import admin
from .models import Sell, Insertedcode, SellWater, ClearCredit, ClearTokenTamp,PostPaidSell

admin.site.register(PostPaidSell)
admin.site.register(SellWater)
admin.site.register(ClearCredit)
admin.site.register(ClearTokenTamp)