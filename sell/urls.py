from django.conf.urls import  url
from . import views
from .views import SellCreate, ArduinoCode, Getsold, CreateArduino, UpdateWaterQuantity

urlpatterns = [
 
    url(r'^home/$', views.home, name='home'),
    url(r'^try/querry/$', views.try_querry, name='try_querry'),
    url(r'^ok$', views.ok, name='home'),
    #this is the url for selling water and create sell water 
    url(r'^create/$', SellCreate.as_view()),
    url(r'^create/arduino/$', CreateArduino.as_view()),
    url(r'^get/$', Getsold.as_view()),
    url(r'^code/$', ArduinoCode.as_view()),
    url(r'^update/water/$', UpdateWaterQuantity.as_view(), name='order_detail'),
    # url(r'^blog/', include('blog.urls')),
]
