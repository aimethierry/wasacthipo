from django.db import models
from client.models import Client
from Arduino.models import ArduinoCreation
from product.models import Product
from django.utils import timezone


# Create seeling model
class Sell(models.Model):
    isthere = models.BooleanField(default=False, blank=True)
    client = models.ForeignKey('client.Client', on_delete=models.CASCADE, null=False, blank=True)
    product = models.ForeignKey('product.Product', on_delete=models.CASCADE)
    arduinoId = models.ForeignKey('Arduino.ArduinoCreation', on_delete=models.CASCADE, default=False, blank=True)
    #arduinoId = models.ForeignKey('Arduino.ArduinoCreation', on_delete=models.CASCADE, null=False, blank=True)
    quantity = models.IntegerField(null=True, blank=True)
    total_price = models.FloatField(null=True, blank=True, default=None)
    code = models.CharField(max_length=8, null=True, blank=True)
    date_created = models.DateTimeField(default=timezone.now)
    consumed = models.IntegerField(null=True, blank=True, default=0)
    
    def __str__(self):
        return "%s"%(self.client)


class SellWater(models.Model):
    isthere = models.BooleanField(default=False, blank=True)
    client = models.ForeignKey('client.Client', on_delete=models.CASCADE, null=True, blank=True)
    product = models.ForeignKey('product.Product', on_delete=models.CASCADE)
    machineId = models.ForeignKey('Arduino.ArduinoCreation', on_delete=models.CASCADE, null=False, blank=True)
    #arduinoId = models.ForeignKey('Arduino.ArduinoCreation', on_delete=models.CASCADE, null=False, blank=True)
    quantity = models.IntegerField(null=True, blank=True)
    total_price = models.FloatField(null=True, blank=True, default=None)
    phone_number =  models.CharField(max_length=255, null=True, blank=True)
    code = models.CharField(max_length=255, null=True, blank=True)
    name = models.CharField(max_length=255, null=True, blank=True)
    path = models.CharField(max_length=255, null=True, blank=True)
    date_created = models.DateTimeField(default=timezone.now)
    consumed = models.IntegerField(null=True, blank=True, default=0)
    
    def __str__(self):
        return "%s"%(self.client)



class Insertedcode(models.Model):
    code = models.CharField(max_length=12, null=True, blank=True)
    date_created = models.DateTimeField(default=timezone.now)
    def __str__(self):
        return "%s"%(self.code)


class ClearTokenTamp(models.Model):
    serial_id = models.CharField(max_length=20, null=True, blank=True)
    user_id = models.CharField(max_length=20, null=True, blank=True)
    meter_id = models.CharField(max_length=20, null=True, blank=True)
    token_type = models.CharField(max_length=20, null=True, blank=True)
    token_insert = models.CharField(max_length=20, null=True, blank=True)
    amount = models.CharField(max_length=20, null=True, blank=True)
    phone_number = models.CharField(max_length=20, null=True, blank=True)
    date_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "%s"%(self.user_id)


class ClearCredit(models.Model):
    serial_id = models.CharField(max_length=20, null=True, blank=True)
    user_id = models.CharField(max_length=20, null=True, blank=True)
    meter_id = models.CharField(max_length=20, null=True, blank=True)
    token_type = models.CharField(max_length=20, null=True, blank=True)
    token_insert = models.CharField(max_length=20, null=True, blank=True)
    amount = models.CharField(max_length=20, null=True, blank=True)
    phone_number = models.CharField(max_length=20, null=True, blank=True)
    date_created = models.DateTimeField(default=timezone.now)
    
    def __str__(self):
        return "%s"%(self.user_id)


class PostPaidSell(models.Model):

    client = models.ForeignKey('client.Client', on_delete=models.CASCADE, null=True, blank=True)
    product = models.ForeignKey('product.Product', on_delete=models.CASCADE)
    machineId = models.ForeignKey('Arduino.ArduinoCreation', on_delete=models.CASCADE, null=False, blank=True)
    name = models.CharField(max_length=20, null=True, blank=True)
    phone_number = models.CharField(max_length=20, null=True, blank=True)
    token = models.CharField(max_length=20, null=True, blank=True)
    status = models.BooleanField(default=False, blank=True)
    quantity = models.FloatField(null=True, blank=True)
    amount = models.FloatField(null=True, blank=True)
    date_created = models.DateTimeField(default=timezone.now, blank=True)
    date_paid = models.DateTimeField(default=timezone.now, blank=True)
    path = models.CharField(max_length=255, null=True, blank=True)

    deviceId = models.CharField(max_length=255, null=True, blank=True)
    
    
    def __str__(self):
        return "%s"%(self.name)


