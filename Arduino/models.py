# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from product.models import WaterCategory



class ArduinoCreation(models.Model):
    prepaid = 'prepaid'
    postpaid ='postpaid'
    method = [
        (prepaid, 'prepaid'),
        (postpaid, 'postpaid'),
    ]
    method = models.CharField( max_length=10, choices=method, null=True, blank=True)

    name = models.CharField(max_length=30, null=True, blank=True)
    code = models.CharField(max_length=12, null=True, blank=True)
    code = models.CharField(max_length=12, null=True, blank=True)
    category = category = models.ForeignKey('product.WaterCategory', on_delete=models.CASCADE, blank=True, null=True)
    date_created = models.DateTimeField(default=timezone.now, blank=True, null=True)
    def __str__(self):
        return "%s"%(self.code)

