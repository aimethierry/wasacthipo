from django.db import models
from django.utils import timezone
from Arduino.models import ArduinoCreation 
# Create client model



class Client(models.Model):
    name = models.CharField(max_length=30, blank=True, null=True)
    path = models.CharField(max_length=30, blank=True, null=True)
    id_number = models.CharField(max_length=30, blank=True, null=True)
    phone_number =  models.CharField(max_length=13, blank=True, null=True)
    province = models.CharField(max_length=30, blank=True, null=True)
    disctrict = models.CharField(max_length=30, blank=True, null=True)
    sector = models.CharField(max_length=30, blank=True, null=True)
    machineId = models.ForeignKey('Arduino.ArduinoCreation', on_delete=models.CASCADE, null=False, blank=True)
    date_created = models.DateTimeField(default=timezone.now, blank=True, null=True)
    def __str__(self):
        return "%s "%(self.name)