from django.conf.urls import  url
from . import views
from .views import ClientCreate

urlpatterns = [
 
    url(r'^home$', views.home, name='home'),
    url(r'^create/', ClientCreate.as_view()),
    # url(r'^blog/', include('blog.urls')),
]
