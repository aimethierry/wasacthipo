from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^sell/$', views.ussdsell, name='order_detail'),  
    url(r'^try/$', views.try_test, name='order_detail'),
    url(r'^redis/$', views.redis, name='order_detail'),
    url(r'^confirm/payment/$', views.confirm, name='order_detail'), 

]
