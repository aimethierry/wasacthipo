# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

# Create your models here.
class passedUssd(models.Model):
    phone_number = models.CharField(max_length=255, null=True, blank=True)
    device_number = models.CharField(max_length=255, null=True, blank=True)
    date_created = models.DateTimeField(default=timezone.now)


    def __str__(self):
        return "%s"%(self.phone_number)