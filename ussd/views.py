# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, HttpResponse
from twilio.rest import Client
from django.db import connection
from django.db.models import Sum, Count
from product.models import Product
from Arduino.models import ArduinoCreation
from report.models import DailyReport
import hashlib
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from sell.models import SellWater , PostPaidSell
from client.models import Client as cl
from client.models import Client as clie
import requests
import time as  t
import uuid
import requests 
import africastalking
import datetime

#for cellery
from .task import sleepy, postPaidUssd, prepaidUssd, passedUssdToken
from ussd.models import passedUssd


# from dashboard.views import CloseOpenValve, DailyReportView


total_quantity = 0
product_id = 0
total_price = 1
sub_text = " "
ussd_device_id = 0
ussd_amount = 0             
ussd_amount_limit  = 0                  #final limit the user used to get money
ussd_open_amount = False                #let the user enter the limit 
ussd_start_amount = 16                  #this is the start bit to get money 
ussd_phone_number = ""                  #this takes the phone number the user is using 
ussd_open_pay = False                   #let the user open the screen for payment 
ussd_allow = False       
ussd_total_price = 0  
ussd_prod_name = ""   
device_code = " "
ussd_tap_water = False
ussd_residential = False
ussd_industry = False
ussd_non_residential = False

postpaidTotalAmount = 0                 #total money of the money that the user is going to pay in the ussd 
ussd_token = 0                          #token to use when going to pay 
postpaid = False                        #for postpaid 
ussd_check_token = False                #this checks if the token inserted is true
ussd_check_moneyPaid = False            #this boolean field check if the money input is equal to the money needed 

ussd_water_quantity = 0                 #user water quantiy that he will get after paying 
#these are headers for mobile money payment 
headers = {'token': 'OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^'}   

# This are user for sendig message 
account_sid = 'AC0821389d7f0df92c5ce9ade09cced9b4'
auth_token = '1d23eaa8917d66ab9b3e0e52680eecd2'
client = Client(account_sid, auth_token)

transaction_id = 0

status_check = False                    #check the status and for the pop
response_status = ""                    #get the response 
totalToPay = 0

#This function sends message to the users
class sendMessage:
    #code is the code sent to the client so that he/she can do something 
    def __init__(self, phone_number, code):
        self.phone_number = phone_number
        self.code = code

    def send(self):
        sms_phone = "+25" + str(self.phone_number) 
        sms_message ="ThiPo code is " + str(self.code)
        url = 'https://mistasms.com/sms/api' 
        payload = {'action': 'send-sms', 'to': sms_phone, 'from': 'goTap', 'sms': sms_message} 
        files = {}
        headers = { 'x-api-key': 'eU5ldGJ5R0ZDSWZLamhhbmRJR0o='} 
        response = requests.request('POST', url=url, headers = headers, data = payload, files = files, allow_redirects=False)
        # headers = headers, data = payload, files = files, allow_redirects=False,
        # timeout=undefined, allow_redirects=false
        print(response.text)

    #post paid users code is the uid code 
    def sendInvoice(self, totalMoney):
        sms_phone = "+25" + str(self.phone_number) 
        sms_message ="Amafaranga mwakoresheje muri uku kwezi ni " + str(totalMoney) + "mwakwishyura mukoresheje code ya " + str(self.code) + "murakoze"

        url = 'https://mistasms.com/sms/api' 
        payload = {'action': 'send-sms', 'to': sms_phone, 'from': 'goTap', 'sms': sms_message} 
        files = {}
        headers = { 'x-api-key': 'eU5ldGJ5R0ZDSWZLamhhbmRJR0o='} 
        response = requests.request('POST', url=url, headers = headers, data = payload, files = files, allow_redirects=False)
        print(response.text)
    
    def paidInvoice(self, totalMoney):
        sms_phone = "+25" + str(self.phone_number) 
        sms_message ="Amafaranga mwishyuye ni " + str(totalMoney) + "mwishyuye mukoresheje code ya " + str(self.code) + "murakoze"
        url = 'https://mistasms.com/sms/api' 
        payload = {'action': 'send-sms', 'to': sms_phone, 'from': 'goTap', 'sms': sms_message} 
        files = {}
        headers = { 'x-api-key': 'eU5ldGJ5R0ZDSWZLamhhbmRJR0o='} 
        response = requests.request('POST', url=url, headers = headers, data = payload, files = files, allow_redirects=False)
        print(response.text)
        return "done well"



    
#this function get the product id by passing name 
class GetProdocuctId:
    def __init__(self, name):
        self.name = name 

    def getId(self):
        prod = Product.objects.all()
        res = 0
        for p in prod:           
            if(str(p.category) == str(self.name)):
                res = int(p.id)
        return res


#This function get montly used amount of water 
class getMonthlyWater:
    def __init__(self, device_id):
        self.device_id = device_id

    def getTotal(self):
        dev_id = GetByDeviceId(str(self.device_id)).getId()
        truncate_date = connection.ops.date_trunc_sql('month', 'date_created')
        qs = SellWater.objects.extra({'month':truncate_date})
        report = qs.values('month').annotate(Sum('quantity')).filter(machineId=int(dev_id))
        new_data = list(report)
        index = 0
        total = 0 
        while index <  len(new_data):
            total = (new_data[index]['quantity__sum'])
            index += 1
        return total


class GetUserName:
    def __init__(self):
        pass

    def GetNames(self, myid):
        name = clie.objects.all()
        uname = ""
        for nm in name:
            print(type(nm.machineId))
            if(str(nm.machineId) == str(myid)):
                uname = nm.name
        return uname


class GetPath:
    def __init__(self):
        pass
    def getPath(self, mymachine):
        name = cl.objects.all()
        upath = ""
        for nm in name:
            if(str(nm.machineId) == str(mymachine)):
                upath = nm.path
        return upath 



#This function save in database 
class saveDatabase:
    def __init__(self, prod_id, dev_id, quantity, phone, code, total, path, uname):
        self.prod_id = prod_id
        self.dev_id = dev_id
        self.quantity = quantity
        self.phone = phone
        self.code = code
        self.total = total
        self.path = path
        self.uname = uname
    

    def save(self):
        prd = Product.objects.get(id=int(self.prod_id))
        mac = ArduinoCreation.objects.get(id=int(self.dev_id))
        save =  SellWater.objects.create(product= prd, code = self.code, quantity = self.quantity, consumed=str(0), machineId= mac, phone_number=self.phone, total_price =float(self.total), path=self.path, name=self.uname)
        return True

    def savePostPaid(self, date, deviceId):
        prd = Product.objects.get(id=int(self.prod_id))
        mac = ArduinoCreation.objects.get(id=int(self.dev_id))
        #check the status in order to avoid saving it twice in the database 
        status = postPaidDatabase().checkStatus(date, deviceId)
        if(str(status) == "False"):
            save = PostPaidSell.objects.create(product=prd, token=self.code, quantity = self.quantity,machineId= mac, phone_number=self.phone, amount = float(self.total), path=self.path, name=self.uname, date_created = date, deviceId = str( deviceId) )
        return True


class postPaidDatabase:

    def __init__(self):
        pass

    def checkStatus(self, date, deviceId):
        res =False
        psells = PostPaidSell.objects.all()
        for pst in psells:
            if( (str(date) == str(pst.date_created.date())) and  (str(pst.deviceId) == str(deviceId))):
                res = True
        return res


#This function is for device api for prepaid
class deviceApi:
    def __init__(self, device_id, quantity):
        self.device_id = device_id
        self.quantity = quantity

    def callApi(self):
        global device_code

        # {
        # unique_code = str(uuid.uuid4().int)[:8]
        # "company_name": "ThiPo",
        # "user_name": "POS1",
        # "password": "123456",
        # "password_vend": "123456",
        # "meter_number": "47500137725",
        # "is_vend_by_unit": true,
        # "amount": 1
        # }

        company_name =  "ThiPo",
        user_name =  "POS1",
        password =  "123456",
        password_vend =  "123456",
        meter_number =  str(self.device_id) 
        is_vend_by_unit =  "true",
        amount = str(self.quantity)

        # serial_id =  "000002"
        # user_id  = "go tap"
        # meter_id = str(self.device_id) 
        # token_type = "CreditToken"
        # amount = str(self.quantity)
        # timestamp = unique_code
        # access_key = "8e6453cd3dd94d66bef416c0b60f8285"
        # md5_obj= hashlib.md5((serial_id + user_id + meter_id + token_type + amount + timestamp + access_key).encode("utf-8")).hexdigest()
        # print(md5_obj)
        
        my_url_device = 'https://prepayment.calinhost.com/api/POS_Purchase'
        request_api_device = requests.post(url=my_url_device, 
                data=({"company_name": company_name, 
                "user_name":user_name, 
                "password" : password, 
                "password_vend":password_vend,
                "meter_number": meter_number,
                "is_vend_by_unit": is_vend_by_unit, 
                "amount":amount}),verify=False)
            
        device_code = str(request_api_device.json()['result']['token'])
        print(device_code)
        return device_code

#This is the class that calls the mobile money payment: and sends the message 
class mobile_payment:
    def __init__(self, phone_number, total_price):
        self.phone_number = phone_number
        self.total_price = total_price

    def pay(self):
        res_from_server = deviceApi(ussd_device_id ,ussd_water_quantity).callApi()
        ussd_allow = False 
        ProductId   = int(GetProdocuctId(ussd_prod_name).getId())
        DeviceId    = int(GetByDeviceId(str(ussd_device_id)).getId())
        Quantity = int(ussd_water_quantity)
        PhoneNumber = str(self.phone_number)
        DeviceToken =  str(res_from_server)
        TotalAmountTopay = float(self.total_price)
        Path = str(GetPath().getPath(str(ussd_device_id))) 
        UserName = str(GetUserName().GetNames(str(ussd_device_id)))
        prepaidUssd.delay(ProductId, DeviceId, Quantity, PhoneNumber, DeviceToken, TotalAmountTopay, Path, UserName)
        return "done well"
    

    def payInvoice(self, token):
        PostpaidId = getClientNumber(str(ussd_device_id)).getPostPaidId(token, self.total_price)
        DeviceToken = token
        PhoneNumber = self.phone_number
        TotalAmountTopay = self.total_price
        postPaidUssd.delay(PostpaidId, DeviceToken, PhoneNumber, TotalAmountTopay)
        return "done well"






#This function gets the id based on device number 
class GetDeviceId:
    def __init__(self, prod_name):
        self.prod_name = prod_name

    def getId(self):
        devi = Product.objects.all()
        #res is the result which is the id of the product
        res = " "
        for p in devi:
            if(p.name == self.prod_name):
                res  = p.id
        return int(res)

    

#This function gets the id based on on product name 
class GetByDeviceId:
    def __init__(self, device_id):
        self.device_id = device_id

    def getId(self):
        devi = ArduinoCreation.objects.all()
        #res is the result which is the ud 
        res = " "
        for d in devi:
            if(d.code == self.device_id):
                res  = d.id
        return int(res)

#looking for device category 
class category_type:
    def __init__(self, device_id, category_type):
        self.device_id = device_id
        self.category_type = category_type

    def search_cat(self):
        global ussd_allow
        cat = ArduinoCreation.objects.all()
        
        category =  ""
        for c in cat:
            if (str(c.category) == self.category_type):
                if(c.code == self.device_id) :
                    ussd_allow = True
                    category = c.category
        return category


#This is for setting price 
class setting_price:
    def __init__(self,quantity, category_type, device_id):
        self.quantity = quantity
        self.category_type = category_type
        self.device_id = device_id

    def calculate_total(self):
        global ussd_water_quantity  
        prod = Product.objects.all()
        devi = ArduinoCreation.objects.all()
        cat = " " 
        price = 1
        cat = category_type(self.device_id, self.category_type).search_cat() 
        # getting the prices based on category

        if(str(cat) == "public_tap"):
            for p in prod:
                if(str(p.category) == str(cat)):
                    product_price = p.price
                    ussd_water_quantity =  int(self.quantity) / product_price

        if(str(cat) == "industry"):
            for p in prod:
                if(str(p.category) == str(cat)):
                    product_price = p.price
                    ussd_water_quantity =  int(self.quantity) / product_price
        return int(ussd_water_quantity)

    

class getClientNumber:
    res = 0 
    def __init__(self, dev_id):
        self.dev_id = dev_id

    def getClientNumbers(self):
        clients = cl.objects.all()
        global res
        for c in clients:
            if(str(self.dev_id) == str(c.machineId)):
                res = c.phone_number
        return res

    #get the token that user has inserted for post paid 
    def checkToken(self, token):
        res = False
        pstpaid = PostPaidSell.objects.all()
        for pst in pstpaid:
            if(pst.token == token):
                res = True
        return res

    

    #check the money needed to pay 
    def checkMoney(self, token, money):
        res = False 
        pstpaid = PostPaidSell.objects.all()
        for pst in pstpaid:
            if(pst.token == token):
                if((float(pst.amount)) <= (float(money))):
                    res = True
        return res

    
    #get postpaid id 
    def getPostPaidId(self, token, money):
        res = 0 
        pstpaid = PostPaidSell.objects.all()
        for pst in pstpaid:
            if(pst.token == token):
                res = pst.id
        return res

    def SendClientPaymentSms(self):
        clients = cl.objects.all()
        global res
        for c in clients:
            print(str(self.dev_id))
            print(str(c.machineId))
            if(str(self.dev_id) == str(c.machineId)):
                res = c.phone_number
                # after getting the number send the sms 
                print("send sms ")


class getDays:
    day = 0
    year = 0
    month = 0 
    clNumber = 0
    def __init__(self, date):
        global day 
        global year 
        global month
        self.date = date
        day = (self.date.day)
        month = (self.date.month)
        year  = (self.date.year)


    #get category so that we can create monthly payment
    def getCategory(self, device_id, method):
        cat = ArduinoCreation.objects.all()
        category =  ""
        for c in cat:
            #for post paid 
            if(method == c.method):
                if(c.code == device_id) :
                    ussd_allow = True
                    category = c.category
        return category



    def getPaymenttDay(self):
        global paymentDay
        paymentDay = datetime.datetime(year, month, 16)  
        dr = DailyReport.objects.all()
        res = 0

        for cl in dr:
            if(str(cl.date_create.date()) ==str(paymentDay.date())):
                res = cl.totalConsumed
                cat = getDays(self.date).getCategory(str(cl.meter_id), "postpaid")
                getPostPayedWater(cat, str(cl.meter_id), res).getTotalPrice(self.date)

              





class getPostPayedWater:
    
    clphoneNumber = 0
    clName = ""
    uniqueCode = 0
    path = ""
    def __init__(self, category_type, device_id, quantity):
        self.category_type = category_type
        self.device_id = device_id
        self.quantity  = quantity

    def getTotalPrice(self, date):
        global totalToPay
        global clphoneNumber
        global clName
        global path
        global uniqueCode
          
        prod = Product.objects.all()
        clName = GetUserName().GetNames(str(self.device_id))
        uniqueCode  = (str(uuid.uuid4().int)[:10])
        
        deviceId = GetByDeviceId(self.device_id).getId()
        devi = ArduinoCreation.objects.all()
        clphoneNumber = getClientNumber(str(self.device_id)).getClientNumbers()
        cat = category_type(str(self.device_id), str(self.category_type)).search_cat() 

        if(str(cat) == "residential"):
            productId = GetProdocuctId(str(cat)).getId()
            path = GetPath().getPath(self.device_id)


            if(((0 <= self.quantity) and (self.quantity <= 5)) ):
                cat = "residential_1" 
                for p in prod:
                    if(str(p.name) == str(cat)):
                        price = p.price
                        totalToPay = price * float(self.quantity)


            if((5 < self.quantity) and (self.quantity <= 20)) :
                cat = "residential_2"        
                for p in prod:
                    if(str(p.name) == str(cat)):
                        price = p.price
                        totalToPay = price * float(self.quantity)
                        

            if( (20 < self.quantity) and (self.quantity <= 50) ):
                cat = "residential_3" 
                for p in prod:
                    if(str(p.name) == str(cat)):
                        price = p.price
                        totalToPay =  price * float(self.quantity)


            if((50 < self.quantity)):   
                cat = "residential_4" 
                for p in prod:
                    print(str(p.name))
                    if(str(p.name) == str(cat)):
                        price = p.price
                        totalToPay =  price * float(self.quantity)

        if(str(cat) == "non_residential"):
            
            if( (0 <= self.quantity) and (self.quantity <= 50) ):
                cat = "non_residential_1" 
                for p in prod:
                    if(str(p.name) == str(cat)):
                        price = p.price
                        totalToPay =  float(self.quantity) * price

            
            if((51 <= self.quantity) ):
                cat = "non_residential_2" 
                for p in prod:
                    if(str(p.name) == str(cat)):
                        price = p.price
                        totalToPay =  float(self.quantity) * price
        
        # print(saveDatabase(productId, deviceId, self.quantity,  clphoneNumber, str(uniqueCode) ,totalToPay, path , clName).savePostPaid(date, self.device_id ))
   



class goToPay:
    def payInvoice(self):
        global ussd_check_token
        global ussd_check_moneyPaid
        global postpaid
        global ussd_allow
        if((ussd_check_token == True) and (ussd_check_moneyPaid ==True) and (ussd_allow == True)):
            print("hello")
            ussd_check_token = False
            ussd_check_moneyPaid = False
            postpaid = False
            ussd_allow = False
            mobile_payment(ussd_phone_number, postpaidTotalAmount).payInvoice(ussd_token)
            print("go pay")

    def prepaidPay(self):
        global ussd_allow
        global ussd_open_pay
        if ussd_open_pay == True:
            ussd_allow = False
            ussd_open_pay = False
            print(ussd_total_price)
            mobile_payment(str(ussd_phone_number), str(ussd_total_price)).pay() 

                    
       
def try_test(request):
    # mobile_payment("0787165015", 100).payInvoice("32423424")
    deviceApi("47500137725", "5").callApi()
    return HttpResponse("hello")




#for testing celery
def redis(request):
    passedUssdToken.delay("0787165", "435567866")
    # print(GetUserName().GetNames(str("47500")))
    return HttpResponse("trying to use hello ")



def confirm(requests):
    print("am")
    return HttpResponse("hello")

@csrf_exempt
def ussdsell(request):
    if request.method == 'POST':
        global sub_text
        global ussd_device_id
        global ussd_amount              #is the amount like the quantity the user will have 
        global ussd_open_amount         
        global ussd_amount_limit        
        global ussd_phone_number        #get phone number of the user
        global ussd_open_pay
        global ussd_prod_name           #get the product name whic is category through ussd 

        global ussd_tap_water 
        global ussd_residential 
        global ussd_industry
        global ussd_non_residential
        global ussd_total_price

        global postpaidTotalAmount      # total amount that postpaid user is going to pay
        global postpaid                 #boolean field for pospaid 
        global ussd_token               #the token that the client insert 
        global  ussd_check_token        #check the token to be inserted if its correct 
        global ussd_check_moneyPaid     #check the money paid
        global ussd_allow


        session_id = request.POST.get('sessionId')
        service_code = request.POST.get('serviceCode')
        phone_number = request.POST.get('phoneNumber')
        text = request.POST.get('text')
        ussd_phone_number = str(format(phone_number[(len(phone_number)-10):len(phone_number)]))
        response = ""

        if text == "":
            response = "CON Murakaza Kuri GoTap \n"
            response += "1. Gura Amazi Mbere \n"
            response += "2. Ishyura amazi wakoresheje \n" 

        
        elif text == "2":
            # mobile_payment("0787165015", 99).payInvoice("32423424")
            # mobile_payment("0787165015", "55").payInvoice("1234567")
            # response = "END GO PAY\n"
            response = "CON Hitama ikiciro \n"
            response += "1. Ayo murugo  \n"
            response += "2.  Ayu bucuruzi \n "
        
        if text == "2*1":   
            ussd_prod_name = "residential"
            response = "CON Shyiramo numero yifatabuguzi (conteri) \n" 
            response += "  "
        
        elif text == "2*1*" + text[4:15]:   
            ussd_device_id = text[4:15]
            cat = category_type(str(ussd_device_id), "residential")
            cat.search_cat()
            response = "CON shyiramo amafaranga  \n" 
            response += " " 
            postpaid = True
      


        if text == "2*2":   
            
            ussd_prod_name = "non_residential"
            response = "CON Shyiramo numero yifatabuguzi (conteri) \n" 
            response += "  "
        
        elif text == "2*2*" + text[4:15]:   
            ussd_device_id = text[4:15]
            cat = category_type(str(ussd_device_id), "non_residential")
            cat.search_cat()
            response = "CON shyiramo amafaranga  \n" 
            response += " " 
            postpaid = True



        # print(len(text))
        elif (text != "")  and (postpaid == True) and (len(text) > 16):
            print("am inside here ")
            counter_limit = 0
            val = False
            
            if(len(text) > 16 and (val== False)):
                once = False
                if(once == False):
                    name = text + "." 
                    print(name)
                    print(text)
                    ussd_amount_limit= name[16:name.find(".")]
                    postpaidTotalAmount = ussd_amount_limit
                    print(postpaidTotalAmount)
                    print("after", text)
                    once = True
                
              
                if text == "2*1*" +  str(ussd_device_id) + "*" + str(1000) :
                    once = True
                    response = "CON shyiramo numero wishyuriraho  \n" 
                    response += " " 

                
                if text == "2*1*" +  str(ussd_device_id) + "*" + str(1000) + "*" + "3088691927":
                    # print(text)
                    # "2*1*" + str(ussd_device_id) + "*" + str(postpaidTotalAmount) + "*" + "123"
                    # text[(16+postpaidTotalAmount) : len(text)]
                    # ussd_token = text[(16+len(str(postpaidTotalAmount))+1) : (16+len(str(postpaidTotalAmount))+11)]
                    ussd_token = 3088691927
                    ussd_check_token =  getClientNumber(str(ussd_device_id)).checkToken(str(ussd_token))
                    ussd_check_moneyPaid = getClientNumber(str(ussd_device_id)).checkMoney(str(ussd_token), "1000")
                    goToPay().payInvoice()
                    response = "END Go pay  \n" 

            
            
            #for industries 
            if text == "2*2*" + str(ussd_device_id) + "*" + str(text[16:20]):
                ussd_amount_limit= text[16:text.find(".")]
                postpaidTotalAmount = ussd_amount_limit
                response = "CON shyiramo numero wishyuriraho  \n" 
                response += " " 
               
            if text == "2*2*" + str(ussd_device_id) + "*" + str(postpaidTotalAmount) + "*" + text[(16+len(str(postpaidTotalAmount))+1) : (16+len(str(postpaidTotalAmount))+11)]+ ".":
                ussd_token = text[(16+len(str(postpaidTotalAmount))+1) : (16+len(str(postpaidTotalAmount))+11)]
                ussd_check_token =  getClientNumber(str(ussd_device_id)).checkToken(str(ussd_token))
                ussd_check_moneyPaid = getClientNumber(str(ussd_device_id)).checkMoney(str(ussd_token), postpaidTotalAmount)
                goToPay().payInvoice()

            # else:
            #     print(postpaidTotalAmount)
            #     print(text)
            #     response = "END nimero mwashyizemo ntabwo ariyo mwongere   \n" 




            # if text == "2*1*" + str(ussd_device_id) + "*" + str(postpaidTotalAmount) + "*" + text[(16+len(str(postpaidTotalAmount))+1) : (16+len(str(postpaidTotalAmount))+11)]+ ".":
            #     ussd_token = text[(16+len(str(postpaidTotalAmount))+1) : (16+len(str(postpaidTotalAmount))+11)]
            #     ussd_check_token =  getClientNumber(str(ussd_device_id)).checkToken(str(ussd_token))
            #     ussd_check_moneyPaid = getClientNumber(str(ussd_device_id)).checkMoney(str(ussd_token), postpaidTotalAmount)
            #     goToPay().payInvoice()
            #     response = "END Go pay  \n" 

            # if text == "2*2*" + str(ussd_device_id) + "*" + str(text[16:20]):
            #     ussd_amount_limit= text[16:text.find(".")]
            #     postpaidTotalAmount = ussd_amount_limit
            #     response = "CON shyiramo numero wishyuriraho  \n" 
            #     response += " " 
               
            # if text == "2*2*" + str(ussd_device_id) + "*" + str(postpaidTotalAmount) + "*" + text[(16+len(str(postpaidTotalAmount))+1) : (16+len(str(postpaidTotalAmount))+11)]+ ".":
            #     ussd_token = text[(16+len(str(postpaidTotalAmount))+1) : (16+len(str(postpaidTotalAmount))+11)]
            #     ussd_check_token =  getClientNumber(str(ussd_device_id)).checkToken(str(ussd_token))
            #     ussd_check_moneyPaid = getClientNumber(str(ussd_device_id)).checkMoney(str(ussd_token), postpaidTotalAmount)
            #     goToPay().payInvoice()

            # else:
            #     response = "END nimero mwashyizemo ntabwo ariyo mwongere   \n"         




        elif text == "1":
            response = "CON Hitama ikiciro \n"
            response += "1. Akazu kamazi  \n"
            response += "2. Inganda \n"

        #public tap
        elif text == "1*1":   
            ussd_tap_water = True
            ussd_prod_name = "public_tap"
            response = "CON Shyiramo numero yifatabuguzi (conteri) \n" 
            response += "  "

        elif text == "1*1*" + text[4:15]:   
            ussd_device_id = text[4:15]
            cat = category_type(str(ussd_device_id), "public_tap").search_cat()
            response = "CON shyiramo amafaranga  \n" 
            response += " " 
            ussd_open_amount = True
            # print(text)



        #for public_tap
        elif text == "1*2":   
            ussd_prod_name = "industry"
            ussd_industry = True
            response = "CON Shyiramo numero yifatabuguzi (conteri) \n" 
            response += "  "

        elif text == "1*2*" + text[4:15]:   
            ussd_device_id = text[4:15]
            cat = category_type(str(ussd_device_id), "industry").search_cat()
            response = "CON shyiramo amafaranga  \n" 
            response += " " 
            ussd_open_amount = True
            # print(text)

 
        #start making payment 
        elif (text != "") and (ussd_open_amount == True) and (ussd_allow == True):
            text = text + "."
            ussd_amount_limit = text.find(".") + 1
            print(ussd_amount_limit)
            if (text == "1*1*" + str(ussd_device_id) + "*" + str(text[ussd_start_amount:ussd_amount_limit])) and (ussd_tap_water == True) :
                ussd_open_pay = True
                ussd_amount = text[ussd_start_amount:(ussd_amount_limit - 1)]
                print(ussd_amount)
                y = setting_price(str(ussd_amount), "public_tap", str(ussd_device_id))
                ussd_water_quantity = y.calculate_total()
                ussd_total_price = ussd_amount
                print(ussd_water_quantity)
                goToPay().prepaidPay()
                response = "END Go pay  \n" 
                
           
           
            #industrial
            if (text == "1*2*" + str(ussd_device_id) + "*" + str(text[ussd_start_amount:ussd_amount_limit])) and (ussd_industry == True):
                ussd_open_pay = True
                ussd_amount = text[ussd_start_amount:(ussd_amount_limit - 1)]
                y = setting_price(str(ussd_amount), "industry", str(ussd_device_id))
                ussd_water_quantity = y.calculate_total()
                ussd_total_price = ussd_amount
                print(ussd_water_quantity)
                goToPay().prepaidPay()
                response = "END Go pay  \n" 
            else:
                response = "END mwakwishyura  \n" 
        return HttpResponse(response)  
    return HttpResponse("response") 
