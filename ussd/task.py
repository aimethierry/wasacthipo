# Create your tasks here
from celery import shared_task
from time import sleep
from ussd.models import passedUssd
from sell.models import SellWater , PostPaidSell
import requests
from product.models import Product
from Arduino.models import ArduinoCreation
import uuid


headers = {'token': 'OINjoOhop()*42CS%EWCSf@4r54%vfds!#gd^'}  


#send the message
class sendMessage:
    #code is the code sent to the client so that he/she can do something 
    def __init__(self, phone_number, code):
        self.phone_number = phone_number
        self.code = code
        
    def send(self):
        sms_phone = "+25" + str(self.phone_number) 
        sms_message ="ThiPo code is " + str(self.code)
        url = 'https://mistasms.com/sms/api' 
        payload = {'action': 'send-sms', 'to': sms_phone, 'from': 'goTap', 'sms': sms_message} 
        files = {}
        headers = { 'x-api-key': 'eU5ldGJ5R0ZDSWZLamhhbmRJR0o='} 
        response = requests.request('POST', url=url, headers = headers, data = payload, files = files, allow_redirects=False)
        # headers = headers, data = payload, files = files, allow_redirects=False,
        # timeout=undefined, allow_redirects=false
        print(response.text)

    def paidInvoice(self, totalMoney):
        sms_phone = "+25" + str(self.phone_number) 
        sms_message ="Amafaranga mwishyuye ni " + str(totalMoney) + "mwishyuye mukoresheje code ya " + str(self.code) + "murakoze"
        url = 'https://mistasms.com/sms/api' 
        payload = {'action': 'send-sms', 'to': sms_phone, 'from': 'goTap', 'sms': sms_message} 
        files = {}
        headers = { 'x-api-key': 'eU5ldGJ5R0ZDSWZLamhhbmRJR0o='} 
        response = requests.request('POST', url=url, headers = headers, data = payload, files = files, allow_redirects=False)
        print(response.text)
        return "done well"




#This is the class that calls the mobile money payment: and sends the message 
class mobile_payment:
    def __init__(self, phone_number, total_price):
        self.phone_number = phone_number
        self.total_price = total_price

    def pay(self,ProductId,DeviceId, Quantity, PhoneNumber, DeviceToken, TotalAmountTopay, Path, UserName):
        # res_from_server = deviceApi(ussd_device_id ,ussd_water_quantity).callApi()
        # sendMessage(str(ussd_phone_number), res_from_server).send()
        # res_from_server = deviceApi(ussd_device_id ,ussd_amount).callApi()
        # saveDatabase(int(GetProdocuctId(ussd_prod_name).getId()), int(GetByDeviceId(str(ussd_device_id)).getId()), int(ussd_amount), str(ussd_phone_number), str(res_from_server), float(ussd_total_price),str(GetPath().getPath(str(ussd_device_id))), str(GetUserName().GetNames(str(ussd_device_id)))).save()
        ussd_allow = False 
        start_payment = False

        my_url = 'http://akokanya.com/mtn-pay/'
        request_api = requests.post(url=my_url, data=({'amount': int(self.total_price), 'code': (str(uuid.uuid4().int)[:7]),
        'phone': str(self.phone_number), "company_name":"baza" }),
        headers=headers, verify=False)
        url_transaction_id = request_api.json()['transactionid']
        transaction_id = int(url_transaction_id)
        sleep(40)
        url = 'http://akokanya.com/api/mtn-integration/' + str(transaction_id)
        response = requests.get(url,  headers=headers, verify=False)
        response_status = response.json()[0]['payment_status']
        print((response_status))
        #successful PENDING PENDING SUCCESSFUL
        if(response_status == "SUCCESSFUL"):
            sendMessage(str(self.phone_number), DeviceToken).send()
            prd = Product.objects.get(id=int(ProductId))
            mac = ArduinoCreation.objects.get(id=int(DeviceId))
            SellWater.objects.create(product= prd, code = DeviceToken, quantity = Quantity, consumed=str(0), machineId= mac, 
            phone_number=PhoneNumber, total_price =float(TotalAmountTopay), path=Path, name=UserName)
        return "done well"
    
    #DeviceToken This is the token the client used when paying 
    def payInvoice(self, PostpaidId, DeviceToken, PhoneNumber, TotalAmountTopay):
        global status_check
        my_url = 'http://akokanya.com/mtn-pay/'
        request_api = requests.post(url=my_url, data=({'amount': int(500), 'code': (str(uuid.uuid4().int)[:7]),
        'phone': str(self.phone_number), "company_name":"baza" }),
        headers=headers, verify=False)
        url_transaction_id = request_api.json()['transactionid']
        transaction_id = int(url_transaction_id)
        print("start paying ")
        url = 'http://akokanya.com/api/mtn-integration/' + str(transaction_id)
        response = requests.get(url,  headers=headers, verify=False)
        response_status = response.json()[0]['payment_status']
        print((response_status))
        sleep(30)
        # SUCCESSFUL PENDING
        if(response_status == "SUCCESSFUL"):
            print("am post paid paying")
            PostPaidSell.objects.filter(id=PostpaidId).update(status=True)
            sendMessage(str(PhoneNumber), DeviceToken).paidInvoice(500)
            print("status changed")
            return True 
        return "done well "


@shared_task
def sleepy(duration):
    sleep(duration)
    return None




#prepaid payment and saving into database 
@shared_task
def prepaidUssd(ProductId, DeviceId, Quantity, PhoneNumber, DeviceToken, TotalAmountTopay, Path, UserName):
    print("am in prepaid")
    mobile_payment(PhoneNumber, TotalAmountTopay).pay(ProductId, DeviceId, Quantity, PhoneNumber, DeviceToken, TotalAmountTopay, Path, UserName)
    return "passed"

#is the unique code he/she used when paying 
@shared_task
def postPaidUssd(PostpaidId, DeviceToken, PhoneNumber, TotalAmountTopay):
    mobile_payment(PhoneNumber, TotalAmountTopay).payInvoice(PostpaidId, DeviceToken, PhoneNumber, TotalAmountTopay)
    return "passed"


@shared_task
def passedUssdToken(phone_number, device_id):
    print("its working")
    passedUssd.objects.create(phone_number=phone_number, device_number=device_id)
    return "passed"