from django.shortcuts import render
from .serializers import Productserializer
from django.http import Http404, HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Product

# Create your views here.
def home(request):
    print("done")
    return HttpResponse("hello")



class ProductCreate(APIView):
    '''
    this function is for creating a client 
    '''
    
    def get(self, request, format=None):
        snippets = Product.objects.all()
        serializer = Productserializer(snippets, many=True)
        return Response(serializer.data)
    
    def post(self, request, format=None):
        serializer = Productserializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        



'''
def get(self, request, format=None):
        snippets = Client.objects.all()
        serializer = Clientserializer(snippets, many=True)
        return Response(serializer.data)
'''