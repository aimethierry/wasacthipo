from django.db import models
from django.utils import timezone
# Create product api 



class WaterCategory(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    date_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "%s"%(self.name)


class Product(models.Model):
    name = models.CharField(max_length=30, blank=True, null=True)
    price = models.FloatField(null=True, blank=True, default=None)
    description = models.CharField(max_length=30,blank=True, null=True)
    category = models.ForeignKey('WaterCategory', on_delete=models.CASCADE, blank=True, null=True)
    date_created = models.DateTimeField(default=timezone.now)
    

    def __str__(self):
        return "%s "%(self.name)
