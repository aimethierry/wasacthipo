from django.contrib import admin
from .models import Product,WaterCategory

admin.site.register(Product)
admin.site.register(WaterCategory)