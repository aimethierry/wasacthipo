from django.conf.urls import  url
from . import views
from .views import ProductCreate

urlpatterns = [
 
    url(r'^home$', views.home, name='home'),
    url(r'^create/', ProductCreate.as_view()),
    # url(r'^blog/', include('blog.urls')),
]
