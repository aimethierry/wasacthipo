# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from client.models import Client
from Arduino.models import ArduinoCreation
from product.models import Product
from django.utils import timezone






# Create your models here.
class DailyReport(models.Model):
    # name = models.CharField(max_length=20, null=True, blank=True)
    meter_id = models.CharField(max_length=20, null=True, blank=True)
    # owner = models.CharField(max_length=20, null=True, blank=True)
    TotalUnitsCounter = models.FloatField(null=True, blank=True)   #water amount purchased
    CurrentCreditRegister = models.FloatField(null=True, blank=True)  #remaining 
    usedwater = models.FloatField(null=True, blank=True)  #used water so far
    totalConsumed =  models.FloatField(null=True, blank=True)  #total used water ie total of used water everyday
    date_create = models.DateTimeField(default=timezone.now)
    
    def __str__(self):
        return "%s"%(self.meter_id)