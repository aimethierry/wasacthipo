# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2019-12-23 06:42
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DailyReport',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('meter_id', models.CharField(blank=True, max_length=20, null=True)),
                ('TotalUnitsCounter', models.FloatField(blank=True, null=True)),
                ('CurrentCreditRegister', models.FloatField(blank=True, null=True)),
                ('usedwater', models.FloatField(blank=True, null=True)),
                ('totalConsumed', models.FloatField(blank=True, null=True)),
                ('date_create', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
    ]
