from django.conf.urls import  url
from . import views

urlpatterns = [
    url(r'^home/$', views.home, name='home'),

    #register new user 
    url(r'^signup/$', views.signup, name='signup'),
    #activate the account
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',views.activate, name='activate'),

    url(r'^device/$', views.device, name='device'),

    url(r'^device/create/$', views.deviceCreate, name='create'),
    url(r'^device/edit/$', views.DeviceEdit, name='edit'),
    
   
    url(r'^category/all/$', views.allCategory, name='edit'),
    url(r'^category/create/$', views.categoryCreate, name='edit'),

    url(r'^client/all/$', views.allclient, name='all'),
    url(r'^client/create/$', views.clientCreate, name='all'),
    url(r'^product/all/$', views.allproduct, name='all'),
    url(r'^create/product/$', views.productCreate, name='all'),
    url(r'^sold/water/$', views.allWater, name='all'),

    url(r'^search/$', views.search, name='all'),

    url(r'^report/$', views.report, name='all'),

    url(r'^postpaid/report/$', views.postPaidReport, name='all'),
    url(r'^closed/postpaid/report/$', views.closedPostPaidDevice, name='all'),

    url(r'^clear/token/tamp/$', views.ClearTokenTamp, name='all'),
    url(r'^clear/credit/$', views.ClearCredit, name='all'),
    url(r'^csv/$', views.csv, name='all'),

    #start page
    url(r'^start/$', views.start, name='start'),
   
]