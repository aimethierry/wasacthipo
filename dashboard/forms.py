from django import forms 
from Arduino.models import ArduinoCreation
from client.models import Client
from product.models import Product,WaterCategory
from sell.models import ClearTokenTamp, ClearCredit, SellWater

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class DeviceForm(forms.ModelForm):
    name = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter a name of device", 
                            "class": "form-control"}
                    ))
    code = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter a code of a device", 
                            "class": "form-control"}
                    ))

    method = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "prepaid or postpaid ?", 
                            "class": "form-control"}
                    ))

    class Meta:
        model = ArduinoCreation
        fields = [
            'name',
            'code',
            'method',     
            'category'   
        ]


class ClientForm(forms.ModelForm):
    name = forms.CharField(label='', 
                widget=forms.TextInput(attrs={'placeholder': "Enter a name of client", "class": "form-control"}))
    id_number = forms.CharField(label='', 
                widget=forms.TextInput(attrs={'placeholder': "Enter a Client Id number", "class": "form-control"}))

    phone_number = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "078******* ", 
                            "class": "form-control"}))

    province = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter a client  province", 
                            "class": "form-control"}))


    disctrict = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter a client district", 
                            "class": "form-control"}))

    sector = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter a client sector", 
                            "class": "form-control"}))

    path = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter a client path", 
                            "class": "form-control"}))

  
    class Meta:
        model = Client
        fields = ['name','id_number', 'phone_number','province','disctrict','sector', 'path','machineId']


class ProductForm(forms.ModelForm):
    name = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter a name of the product", 
                            "class": "form-control"}
                    ))
    price = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter a price of a product", 
                            "class": "form-control"}
                    ))

    description = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter a small description of the  product", 
                            "class": "form-control"}
                    ))
    class Meta:
        model = ArduinoCreation
        fields = ['name','price',  'description']




class ClearTokenTampForm(forms.ModelForm):
    serial_id = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter  the serial_id", 
                            "class": "form-control"}
                    ))
    user_id = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter user_id", 
                            "class": "form-control"}
                    ))

    meter_id = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter the meter_id", 
                            "class": "form-control"}
                    ))

    token_type = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter the token_type", 
                            "class": "form-control"}
                    ))
    phone_number = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter the phone_number", 
                            "class": "form-control"}
                    ))

    class Meta:
        model = ClearTokenTamp
        fields = ['serial_id','user_id',  'meter_id', 'token_type', 'amount', 'phone_number']


class ClearCreditForm(forms.ModelForm):
    serial_id = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter  the serial_id", 
                            "class": "form-control"}
                    ))
    user_id = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter user_id", 
                            "class": "form-control"}
                    ))

    meter_id = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter the meter_id", 
                            "class": "form-control"}
                    ))

    token_type = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter the token_type", 
                            "class": "form-control"}
                    ))
    phone_number = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter the phone_number", 
                            "class": "form-control"}
                    ))

    class Meta:
        model = ClearCredit
        fields = ['serial_id','user_id',  'meter_id', 'token_type', 'phone_number', 'amount']



class SellWaterForm(forms.ModelForm):
    client = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter  the client id", 
                            "class": "form-control"}
                    ))
    product = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter product id ", 
                            "class": "form-control"}
                    ))

    machineId = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter the machineId id ", 
                            "class": "form-control"}
                    ))

    consumed = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter amount consumed", 
                            "class": "form-control"}
                    ))
    class Meta:
        model = SellWater
        fields = ['client','product',  'machineId', 'quantity', 'consumed']




class CategoryForm(forms.ModelForm):
        name = forms.CharField(label='', 
                widget=forms.TextInput(
                        attrs={'placeholder': "Enter  the client id", 
                            "class": "form-control"}
                    ))
        class Meta:
            model = WaterCategory
            fields = ['name']


class SignupForm(UserCreationForm):
    email = forms.EmailField(max_length=200, help_text='Required')
    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')