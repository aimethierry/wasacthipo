# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from .forms import DeviceForm, ClientForm, ProductForm, ClearCreditForm, ClearTokenTampForm, CategoryForm, SignupForm
from Arduino.models import ArduinoCreation
from client.models import Client as cl
from product.models import Product, WaterCategory
from sell.models import SellWater, PostPaidSell
from sell.models import SellWater as s
from sell.models import ClearTokenTamp, ClearCredit, PostPaidSell
from django.http import HttpResponse
from django.contrib.auth import login, authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from dashboard.token import account_activation_token
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from report.models import DailyReport
import uuid
import urllib3 
urllib3.disable_warnings()
import hashlib
import requests
from twilio.rest import Client
import datetime
import json
from django.shortcuts import render_to_response
from django.template import RequestContext
from ussd.views import getDays, getClientNumber, sendMessage
import pandas as pd

account_sid = 'AC0821389d7f0df92c5ce9ade09cced9b4'
auth_token = '1d23eaa8917d66ab9b3e0e52680eecd2'
client = Client(account_sid, auth_token)

from django.db.models import Q


# Create your views here.


#This function sends message to the users
class sendMessage:
    def __init__(self, phone_number, code):
        self.phone_number = phone_number
        self.code = code

    #for the prepaid method 
    def send(self):
        sms_phone = "+25" + str(self.phone_number) 
        #the code and the message that is sent to the client 
        sms_message = self.code 
        url = 'https://mistasms.com/sms/api' 
        payload = {'action': 'send-sms', 'to': sms_phone, 'from': 'goTap', 'sms': sms_message} 
        files = {}
        headers = { 'x-api-key': 'eU5ldGJ5R0ZDSWZLamhhbmRJR0o='} 
        response = requests.request('POST', url=url, headers = headers, data = payload, files = files, allow_redirects=False)
    
        print(response.text)

    #for post paid code is the token that is sent to the client 
    def sendInvoice(self, totalAmount, quantityUsed, deviceId):
        sms_phone = "+25" + str(self.phone_number) 
        sms_message ="Musabwe kwishyura amafaranga  " + str(totalAmount) + " mwakoresheje kuri conteri numero " + str(deviceId) + " mushobora kwishyura mukoresheje code " + str(self.code) + " igihe ntarengwa ni tariki 10 zukwezi gutaha murakoze "
        url = 'https://mistasms.com/sms/api' 
        payload = {'action': 'send-sms', 'to': sms_phone, 'from': 'goTap', 'sms': sms_message} 
        files = {}
        headers = { 'x-api-key': 'eU5ldGJ5R0ZDSWZLamhhbmRJR0o='} 
        response = requests.request('POST', url=url, headers = headers, data = payload, files = files, allow_redirects=False)
        return True
    






def home(request):
    return render(request, 'home.html',)

def device(request):
    name = ArduinoCreation.objects.all()
    context = {
        'object_list': name
    }
    return render(request, 'allDevice.html', context)
    

def deviceCreate(request):
    form = DeviceForm(request.POST)
    if request.method =='POST':
        form = DeviceForm(request.POST)
        if form.is_valid():
            instance = form.save()
            instance.save()
            print("succeed")
            return redirect('/dashboard/device/create/')
    else:
        context = {'form':form}
    return render(request, 'createDevice.html',{'form': form} )


def allclient(request):
    name = cl.objects.all()
    context = {'object_list': name}
    return render(request, 'allCleint.html', context)


class DeviceApi:
    #This is some common data on device Api data 
    serial_id = "000002"
    unique_code = str(uuid.uuid4().int)[:8]
    user_id  = "go tap"
    amount = "0"
    timestamp = unique_code
    access_key = "8e6453cd3dd94d66bef416c0b60f8285"




def DeviceEdit(request, id=None):
    ok = ArduinoCreation.objects.get(id=id)
    form = ArduinoCreation(request.POST, request.FILES)
    if request.method =="POST":
        form = ArduinoCreation(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save()
            instance.save()
            print("succeed")
            return redirect('dashoard/device/')
    else:
        context = {
            'instance':ok,
            'form':form
        }
    return render(request,"device_edit.html", {'instance':ok, 'form':form})


def clientCreate(request):
    form = ClientForm(request.POST)
    if request.method =='POST':
        form = ClientForm(request.POST)
        if form.is_valid():
            instance = form.save()
            instance.save()
            print("succeed")
            return redirect('/dashboard/client/create/')
    else:
        context = {'form':form}
    return render(request, 'createClient.html',{'form': form} )

def productCreate(request):
    form = ProductForm(request.POST)
    if request.method =='POST':
        form = ProductForm(request.POST)
        if form.is_valid():
            instance = form.save()
            instance.save()
            print("succeed")
            return redirect('/dashboard/product/create/')
    else:
        context = {'form':form}
    return render(request, 'createProduct.html',{'form': form})



def ClearTokenTamp(request):
    form = ClearTokenTampForm(request.POST)
    if request.method =='POST':
        form = ClearTokenTampForm(request.POST)

        # {
        # "company_name": "ThiPo",
        # "user_name": "Maintenance1",
        # "password": "123456",
        # "meter_number": "47500137725"
        # }  

        if form.is_valid():   
            instance = form.save(commit=False) 
            phone = form.cleaned_data['phone_number']
            meter_id = form.cleaned_data['meter_id']
            
            my_url_device = 'https://prepayment.calinhost.com/api/Maintenance_ClearTamper'
            request_api_device = requests.post(url=my_url_device, 
            data=({"company_name": "ThiPo" ,"user_name": "Maintenance1" , "password": "123456", "meter_number": str(meter_id)}),verify=True)
            print(request_api_device.json())
            mydata = "Clear Temper Code is " +  str(request_api_device.json()['result']) 
            print(mydata)

            instance.token_insert = "123"
            instance.amount = str(0)
            instance.save()
            sendMessage(phone, mydata).send()
            
            print(mydata)
            print("succeed")
            return redirect('/dashboard/clear/token/tamp/')
    else:
        context = {'form':form}
    return render(request, 'clearTokenTemper.html',{'form': form })



def ClearCredit (request):
    form = ClearCreditForm(request.POST)

    # {
    # "company_name": "ThiPo",
    # "user_name": "Maintenance1",
    # "password": "123456",
    # "meter_number": "47500137725"
    # }

    if request.method =='POST':
        form = ClearCreditForm(request.POST)
        if form.is_valid():        
            instance = form.save(commit=False)   
            phone = form.cleaned_data['phone_number'] 
            serial_id = "ThiPo"
            user_id  = "Maintenance1"
            meter_id = form.cleaned_data['meter_id']
            my_url_device = 'https://prepayment.calinhost.com/api/Maintenance_ClearCredit'
            request_api_device = requests.post(url=my_url_device, 
            data=({"company_name": serial_id ,"user_name": user_id , "password": "123456", "meter_number": meter_id}),verify=True)

            tokentLock = str(request_api_device.json()['result'])
            
            instance.token_insert = "123"
            mydata =  "Thipo Locking code is " + tokentLock
            instance.amount = str(0)
            instance.save()
            sendMessage(phone, mydata).send()
            print(mydata)
            print("succeed")
            return redirect('/dashboard/clear/credit/')
    else:
        context = {'form':form}
    return render(request, 'clearCredit.html',{'form': form })




def allproduct(request):
    name = Product.objects.all()
    context = {'object_list': name}
    return render(request, 'allProduct.html', context)


def allWater(request):
    name = s.objects.all()
    context = {'object_list': name}
    return render(request, 'allSoldWater.html', context)
    
def search(request):  
    name = s.objects.all()
    query  = request.GET.get("q")   
    res  = name.filter(Q(phone_number__icontains=query) | Q(name__icontains=query) | Q(path__icontains=query)  )
    # if res.count() == 0:
    #    context = {'object_list': res}
    #     return render(request, 'search.html', context)
    # else:
    #     print("does not exist")     print(res)
    # 
    context = {'object_list': res}
    return render(request, 'search.html', context)
    # return render(request, 'search.html')


def allCategory(request):
    name = WaterCategory.objects.all()
    context = {'object_list': name}
    return render(request, 'allcategory.html', context)


def categoryCreate(request):
    form = CategoryForm(request.POST)
    return render(request, 'createCategory.html',{'form': form})


def csv(request):
    return HttpResponse("don well")


def signup(request):
    formsignup = SignupForm(request.POST)
    print(1)
    if request.method == "POST":
        formsignup = SignupForm(request.POST)
        print(2)
        #check if the form is valid 
        if formsignup.is_valid():
            print(3)
            user = formsignup.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            mail_subject = 'Activate your blog account.'
            message = render_to_string('acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid':urlsafe_base64_encode(force_bytes(user.pk)),
                'token':account_activation_token.make_token(user),
            })
            print(4)
            to_email = formsignup.cleaned_data.get('email')
            email = EmailMessage(
                        mail_subject, message, to=[to_email]
            )
            email.send()
            print(5)
            return HttpResponse('Please confirm your email address to complete the registration')
        else:
            form = SignupForm()

    return render(request, 'signup.html', {'form': formsignup})

#activate the link
def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        # return redirect('home')
        # return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
        return redirect('/login')
    else:
        return HttpResponse('Activation link is invalid!')
    return redirect('/login')




        


class CloseOpenValve:
    def __init__(self, meterId, status):
        self.meterId = meterId
        self.status = status
        # "Close Valve"
    def Openclose(self):
        payload = {
            "CompanyName": "ThiPo",
            "UserName": "AMR02",
            "Password": "123456",
            "MeterNo": str(self.meterId),
            "DataItem": str(self.status)
        }
        my_url = 'https://prepayment.calinhost.com/api/COMM_RemoteControl'
        request_api = requests.post(url=my_url, data=payload, verify=False)
        print(request_api.json())
        return "done well"



class DailyReportView:
    day = 0
    year = 0
    month = 0
    
    def __init__(self, date):
        global day
        global year
        global month
        self.date = date
        day = (self.date.day)
        year = (self.date.month)
        month = (self.date.year)

    def get_id(self, d_id):
        rep = DailyReport.objects.all()
        res = 0
        for dt in rep:
            if(dt.meter_id == d_id):
                res = dt.id
        return res


    #check if there is an element in the database and 
    def check_saved(self,dev_id):
        rep = DailyReport.objects.all()
        
        res = False
        for dt in rep:
            if(dt.meter_id == dev_id):
                res = True
        return res

    #update if the time is differenct
    def checkUpdate(self, dev_id):
        rep = DailyReport.objects.all() 
        res = False
        for dt in rep:
            if((str(dt.date_create.date()) == str(self.date)) and (dt.meter_id == dev_id) ):
                res = True
        return res

    
    def dreport(self):  
        devices = ArduinoCreation.objects.all()
        for ard in devices:
            if(ard.method == "postpaid"):

                payload ={
                "CompanyName": "ThiPo",
                "UserName": "AMR02",
                "Password": "123456",
                "QueryList": 
                [{
                    "MeterNo": str(ard.code),
                    "Year": 2019,
                    "Month": 12,
                    "Day": 12
                    }]
                }
                headers = {'Content-Type': 'application/json'}   #payment headers
                x =  json.dumps(payload)
                request_api = requests.post(url="https://prepayment.calinhost.com/api/COMM_DailyData", data = x, headers=headers)
                x = request_api.json()['Result']
                data = pd.DataFrame(x)
                TotalUnitsCounter = data['TotalUnitsCounter'].as_matrix()
                TotalUnitsCounter  = ''.join(str(x) for x in TotalUnitsCounter)  
                meter_number = data['MeterNo'].as_matrix()
                meter_number = ''.join(str(x) for x in meter_number) 
                CurrentCreditRegister = data['CurrentCreditRegister'].as_matrix()
                CurrentCreditRegister = ''.join(str(x) for x in CurrentCreditRegister) 
                #get the value of water used by on each post paid id 
                used_water = (float)(TotalUnitsCounter) - (float)(CurrentCreditRegister)
                getReportId = DailyReportView(self.date).get_id(str(ard.code))
                if(getReportId == 0):
                    totalConsumed = 0 + used_water
                    print(used_water)
                    print(totalConsumed)
                    DailyReport.objects.create(meter_id=meter_number , TotalUnitsCounter = float(TotalUnitsCounter), CurrentCreditRegister = float(CurrentCreditRegister), usedwater = float(used_water), totalConsumed = totalConsumed,date_create = datetime.datetime.now())
   
                if(getReportId != 0):    
                    totalConsumed = DailyReport.objects.get(id=int(str(getReportId))).totalConsumed 
                

                    #check of the update of the id instead of creating a new one 
                    statusUpdate = DailyReportView(self.date).checkUpdate(str(ard.code))
                    if(statusUpdate == False):
                        totalConsumed = totalConsumed + used_water
                        DailyReport.objects.create(meter_id=meter_number , TotalUnitsCounter = float(TotalUnitsCounter), CurrentCreditRegister = float(CurrentCreditRegister), usedwater = float(used_water), totalConsumed = totalConsumed,date_create = datetime.datetime.now())
                        # DailyReport.objects.filter(pk=getReportId).update(totalConsumed= totalConsumed, date_create = datetime.datetime.now())
               





# #call the function globally so that everytime it will be recodring what is happening to get the report everydat
# DailyReportView(datetime.datetime.now().date()).dreport()
def report(request):
    DailyReportView(datetime.datetime.now().date()).dreport()
    name = DailyReport.objects.all()
    context = {'object_list': name}
    return render(request, 'dailyreport.html', context)



class makeInvoice:
    global paymentDay
    def __init__(self):
        pass

    def getMonthlyInvoice(self):
        global paymentDay
        pspaid = PostPaidSell.objects.all()
        now = datetime.datetime.now().date()
        year = now.year
        month = now.month
  
        
        paymentDay =  str(now)
        # print(paymentDay)
        for pst in pspaid:
            date = str(pst.date_created.date())
            print(date)
            if(date == paymentDay):
                print("goint to send the message ")
                # print("make send invoice ")
                # print(date)
                clNumber = getClientNumber(pst.deviceId).getClientNumbers()
                # print(clNumber)
                # print(pst.deviceId)
                # print(pst.token)
                # print(pst.amount)
                # print(pst.quantity)
                sendMessage(clNumber, pst.token).sendInvoice(pst.amount, pst.quantity, pst.deviceId)
        
    def closePostPaidDevice(self):
        pspaid = PostPaidSell.objects.all()
        # print("closing the device")
        # print(CloseOpenValve("47500137725", "Open Valve").Openclose())


        for pst in pspaid:
            if(str(pst.status) == "False"):
                print(pst.deviceId)
                CloseOpenValve(str(pst.deviceId), "Close Valve").Openclose()








today_date = datetime.datetime.now().date()
today_year = today_date.year
today_month = today_date.month
paymentDay = datetime.datetime(today_year, today_month, 16)

if(str(today_date) == str(paymentDay)):
    print("send the invoice ")
    makeInvoice().getMonthlyInvoice()
    getDays(datetime.datetime.now().date()).getPaymenttDay()
    makeInvoice().closePostPaidDevice()

    

#make it global so that we can avoid 
# makeInvoice().getMonthlyInvoice()
# getDays(datetime.datetime.now().date()).getPaymenttDay()


# monthyly post paid report 
def postPaidReport(request):
    print("send the message ")
    makeInvoice().getMonthlyInvoice()
    getDays(datetime.datetime.now().date()).getPaymenttDay()
    name = PostPaidSell.objects.all()
    context = {'object_list': name}
    return render(request, 'postpaidReport.html', context)

def closedPostPaidDevice(requests):
    # if(str(today_date) == str(paymentDay)):
    #     makeInvoice().closePostPaidDevice()
    # makeInvoice().getMonthlyInvoice()
    makeInvoice().closePostPaidDevice()
    name = PostPaidSell.objects.filter(status=False)
    context = {'object_list': name}
    return render(requests, "closedPostPaidDevice.html", context)



# def handler404(request, *args, **argv):
#     response = render_to_response('404.html', {},
#                                   context_instance=RequestContext(request))
#     response.status_code = 404
#     return response


def handler404(request, exception, template_name="404.html"):
    response = render_to_response("404.html")
    response.status_code = 404
    return HttpResponse("start")

def start(requests):
    return HttpResponse("start")